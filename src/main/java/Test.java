import org.jivesoftware.smack.*;
import org.jivesoftware.smack.packet.Message;

public class Test {

    public static void main(String args[]) throws XMPPException {

        ConnectionConfiguration config = new ConnectionConfiguration("127.0.0.1", 5222);	//新建连接配置对象，设置服务器IP和监听端口

        XMPPConnection connection = new XMPPConnection(config);								//得到基于xmpp协议的连接对象

        connection.connect();		//连接服务器

        connection.login("admin", "admin");		//利用用户名和密码登录

        ChatManager cm = connection.getChatManager(); 	//取得聊天管理器

        Chat chat = cm.createChat("test@etiantian", new MessageListener() {
            @Override
            public void processMessage(Chat chat, Message message) {
                System.out.println(chat.getParticipant() + ":" + message.getBody());
                try {
                    chat.sendMessage("你刚才说的是："+message.getBody());		//发送消息
                } catch (XMPPException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });	//得到与另一个帐号的连接，这里是一对一,@后面是你安装openfire时注册的域


		/*
		 * 添加监听器
		 *//*
        cm.addChatListener(new ChatManagerListener() {

            @Override
            public void chatCreated(Chat chat, boolean create) {
                chat.addMessageListener(new MessageListener() {

                    @Override
                    public void processMessage(Chat chat, Message msg) {

                    }
                });
            }
        });*/

        chat.sendMessage("你好");		//发送消息

        while(true){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //connection.disconnect();//断开连接


    }

}